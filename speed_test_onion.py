#!/usr/bin/env python3.11

import sys
try:
    import requests
except ImportError:
    sys.exit("Please install missing library: python3.12 -m pip install --break-system-packages requests")
try:
    import argparse
    from argparse import RawTextHelpFormatter
except ImportError:
    sys.exit("Please install missing library: python3.12 -m pip install --break-system-packages argparse")

def measure_internet_speed(num_tests):
    sites = [
        "https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion",
        "https://invidious.flokinet.to/feed/popular"
    ]

    if num_tests > len(sites):
        num_tests = len(sites)

    download_speeds = []

    for i in range(num_tests):
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36"}
        proxies = {"http": "socks5h://127.0.0.1:9050", "https": "socks5h://127.0.0.1:9050"}
        try:
            response = requests.get(sites[i], headers=headers, proxies=proxies)
            if response.status_code == 200:
                download_speed = len(response.content)
                download_speeds.append(download_speed)
        except requests.exceptions.RequestException as e:
            print(f"Failed to establish a connection to {sites[i]}: {str(e)}")

    try:
        if download_speeds:
            average_speed = sum(download_speeds) / len(download_speeds)
            print(f"Average Download Speed: {average_speed} bytes/s ({average_speed / 1024:.0f} kB/s)")
        else:
            print("No internet speed measurements available.")
    except:
        sys.exit("Please install missing library: python3.12 -m pip install --break-system-packages pysocks")

try:
    parser = argparse.ArgumentParser(description="Measure internet download speed using Torsocks.",
                                     formatter_class=RawTextHelpFormatter,
                                     epilog="Examples:\n"
                                            "./download_speed.py --precision 3\n"
                                            "./download_speed.py --precision 2\n"
                                            "./download_speed.py -p 4\n"
                                            "./download_speed.py")

    parser.add_argument("--precision", "-p", type=int, default=1, help="Number of sites to test (default: 1)")
    args = parser.parse_args()

    measure_internet_speed(args.precision)

except KeyboardInterrupt:
    sys.exit()

